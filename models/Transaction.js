// Transaction.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Contact
let Transaction = new Schema({
        "blood_group": {"type" : String},
		"donor_count": {"type": Number},
		"within_hours": {"type": Number},
		"address": {"type": String},
        "latitude": {"type" : Number},
        "longitude": {"type" : Number}
    },
    {
        collection: 'transaction'
    });

module.exports = mongoose.model('transaction', Transaction);