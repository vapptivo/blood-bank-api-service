// Contact.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Contact
let Contact = new Schema({
        "first_name": {"type" : String},
        "last_name": {"type" : String},
        "password": {"type" : String},
        "email": {"type" : String},
        "phone_number": {"type" : String},
        "blood_group": {"type" : String},
        "gender": {"type" : String},
        "latitude": {"type" : Number},
        "longitude": {"type" : Number}
    },
    {
        collection: 'contact'
    });
module.exports = mongoose.model('contact', Contact);