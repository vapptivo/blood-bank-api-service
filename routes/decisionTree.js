var DecisionTree = require('decision-tree');
class SpeechDecision{
    constructor() {
         this.training_data = [
            {"state":"1001", "status":"601", "result":"1x601k"},
            {"state":"1001", "status":"602", "result":"1x601k"},
            {"state":"1001", "status":"603", "result":"1x601k"},
            {"state":"1001", "status":"604", "result":"1x601k"},
            {"state":"1001", "status":"701", "result":"1x001d"},
            {"state":"1001", "status":"702", "result":"1x001d"},
            {"state":"1001", "status":"703", "result":"1x001d"},
            {"state":"1003", "status":"701", "result":"1x001d"},
            {"state":"1003", "status":"702", "result":"1x001d"},
            {"state":"1003", "status":"703", "result":"1x001d"},
            {"state":"1002", "status":"701", "result":"1x001d"},
            {"state":"1002", "status":"702", "result":"1x001d"},
            {"state":"1002", "status":"703", "result":"1x001d"},
            {"state":"1002", "status":"601", "result":"1x801x"},
            {"state":"1002", "status":"602", "result":"1x801x"},
            {"state":"1002", "status":"603", "result":"1x801x"},
            {"state":"1002", "status":"604", "result":"1x801x"},
            {"state":"1003", "status":"601", "result":"1x501j"},
            {"state":"1003", "status":"602", "result":"1x501j"},
            {"state":"1003", "status":"603", "result":"1x501j"},
            {"state":"1003", "status":"604", "result":"1x501j"}
        ];
        this.result_text={
            "200":"Hi, this is Alice from @#bloodbank#@ blood bank. Am I Speaking to @#user#@?",
            "1x601k":"Hi @#user#@, As you have registered in Red App to donate blood during emergency,we are calling you. Are you willing to donate blood?",
            "1x603h":"",
            "1x604l":"",
            "1x001d":"Thank you have a nice day.",
            "1x801x":"So Will you be available within @#time#@ hours at @#bloodbank#@ blood bank?",
            "1x802s":"",
            "1x803g":"",
            "1x501j":"Thank you, will send you the adddress in sms, its nice speaking to you. Have a nice day.",
            "1x402r":"",
            "1x403u":""
        }
        this.phrase={
                "greet":"200",
                "yes":"601",
                "yea":"602",
                "yeah":"602",
                "sure":"603",
                "yep":"603",
                "ok":"604",
                "no":"701",
                "not":"702",
                "sorry":"703"
        }
        this.contextCode={
            "initial":"1001",
            "interm":"1002",
            "final":"1003"
        }

      }
    
      predictOutputSpeech(context,speech){
        if(speech=="greet"){
            return  this.result_text["200"];
        }
        else{
        let class_name = "result";
        let status = this.phrase[speech];
        let state = this.contextCode[context];
        console.log(state);
        console.log(status);
        let features = ["state", "status"];
        let dt = new DecisionTree(this.training_data, class_name, features);
        let predicted_class = dt.predict({
            state: state ,
            status: status 
        });
        console.log(this.result_text[predicted_class]);
         return  this.result_text[predicted_class];
          }
      }

      checkContextAvailable(speech){
          console.log("checkContextAvailable::"+speech && this.phrase[speech])
          return speech && this.phrase[speech] ? true : false;
      }
     
      getTokensFromSpeech(speech){
        const regex = /[a-zA-Z]+/g;
        let m;
        let speechArray=[];
        while ((m = regex.exec(speech)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                console.log(`Found match, group ${groupIndex}: ${match}`);
                speechArray.push(match);
            });
            
        }
        return speechArray;
      }


}
module.exports = SpeechDecision;
