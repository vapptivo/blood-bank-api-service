// bloodbank.route.js

const express = require('express');
const app = express();
//const routes = express.Router();

var speechDecision = require('./decisionTree');

const accountSid = 'ACe3771e0463140f0e71b4e17e6ab3e22f';
const authToken = '6650b4a2a7f17cf9804580b3a5314f01';
const client = require('twilio')(accountSid, authToken);
const twilioAPIURL = "/api/bloodBankAppAPI/";
const BASE_URL = "http://3.93.81.11:3000";
const speechDecisionObj = new speechDecision();

app.get("http://localhost:3000" + twilioAPIURL+'testcall', function (req, res) {
			res.status(200).json({'status': 'success'});
		});

class TwilioResponse{
	constructor(user,time,bloodBank,toNumber){
		 this.user= user;
		 this.time= time;
		 this.bloodBank=bloodBank;
		 this.toNumber = toNumber;
	}
	init(){
		this.twilioCreateCall();
		this.twilioMakeCall();
		this.twilioStatusCallback();
		this.twilioPartialCallback();
		
		this.sampleapi();
	}
	
	sampleapi() {
		
	}
	
	twilioCreateCall(){


		client.calls
		.create({
				url: BASE_URL+twilioAPIURL+'makeCall?state=trigger&speechResp=greet',
				to: this.toNumber,
				from: '+17013472763',
				statusCallback: BASE_URL+twilioAPIURL+'statusCallback',
				statusCallbackEvent: ['ringing','initiated','completed'],
				statusCallbackMethod: 'POST',
				method:'POST'
		})
		.then(call => console.log("makeCall::"+call.status));
	}

	twilioMakeCall(){
	   let getMakeCallResponse = this.getMakeCallResponse;
	   let user = this.user
	   let time=  this.time;
	   let bloodBank = this.bloodBank;
	   let url = BASE_URL+twilioAPIURL;
	   
		app.post(twilioAPIURL+'makeCall', function (req, res) {
			console.log('Triggering makeCall');
			console.log(req.query.state);
			let state = req.query.state;
			let speech = req.query.speechResp;
			let say="";
			if(speech){
				let speechArray = speechDecisionObj.getTokensFromSpeech(speech);
				console.log("speechArray"+speechArray.length);
				if(speechArray.length == 1)
					say = getMakeCallResponse(state,speechArray[0].toLowerCase(),user,time,bloodBank);
				else{
				   for(let i=0;i<speechArray.length;i++){
					 if(speechDecisionObj.checkContextAvailable(speechArray[i])){
						say = getMakeCallResponse(state,speechArray[i].toLowerCase(),user,time,bloodBank); 
						break;
					 }
				   }
				}
			}
			else{
				say="Sorry, Could you please come again?";
			}
			let responseText="<Response><Gather input='speech' action='"+ url
			+ "callback' partialResultCallback='" + url+ "partialcallback?state="
			+ state+"'><Say voice='alice'>" + say + "</Say></Gather></Response>";
			console.log(responseText);
			console.log("makeCall Response>>"+responseText);
			res.set('Content-Type', 'text/xml');
			res.send(responseText);
		});

	}

	twilioStatusCallback(){
		app.post(twilioAPIURL+'statusCallback', function (req, res) {
			console.log("Got a StatusCallBack Request");
			console.log(req.body.CallStatus);
			res.set('Content-Type', 'text/xml');
			if(req.body.CallStatus == 'completed'){
				res.send('<Response><Hangup></Hangup></Response>');
			}
			 else{
				res.send('<Response></Response>');
			 }   
			
		   
		});

		app.post(twilioAPIURL+'callback', function (req, res) {
			console.log("Speech result ::"+req.body.SpeechResult);
			console.log('Triggering callback');
			res.set('Content-Type', 'text/xml');
			res.send('<Response><Say voice="alice">Thank you. Have a nice day.</Say></Response>');
		});

	}

	twilioPartialCallback(){
		app.post(twilioAPIURL+'partialcallback', function (req, res) {
			console.log('Triggering partialcallback');
			let state = req.query.state ;
			console.log("state::"+state);
			if(state == "initial")
				 state="interm";
			else if(state == "interm")
				 state="final";
			else
				state= "initial";

			let se_input = req.body.StableSpeechResult;
			let ue_input = req.body.UnstableSpeechResult;
			console.log("Stable input ::"+se_input);
			console.log("UnStable input::"+ue_input);
			let speechResult = se_input != ""?se_input : ue_input != "" ? ue_input :"";
			console.log("sid"+req.body.CallSid) ;  
			client.calls(req.body.CallSid)
				  .update({method: 'POST', url: BASE_URL+twilioAPIURL+'makeCall?state='+state+'&speechResp='+speechResult})
				  .then(call => console.log(call.to));
				 
			res.set('Content-Type', 'text/xml');
			res.send('<Response></Response>');
		});

	}
	 getMakeCallResponse(context,speech,user,time,bloodBank){
		
		console.log("context and speech>>"+context,speech);
		let speechOut = speechDecisionObj.predictOutputSpeech(context,speech);
		console.log(speechOut);
		if(speechOut != null && speechOut != ""){
			   speechOut = speechOut.includes("@#user#@") ? speechOut.replace("@#user#@",user):speechOut;
			   speechOut = speechOut.includes("@#time#@") ? speechOut.replace("@#time#@",time):speechOut;
			   speechOut = speechOut.includes("@#bloodbank#@") ? speechOut.replace("@#bloodbank#@",bloodBank):speechOut;
		}
		else {
			speechOut="Sorry, Could you please come again?";
		}
		console.log(speechOut);
		return speechOut;
	}
	} 


module.exports = TwilioResponse;
 
// var twilioResp = new TwilioResponse("Karuna","4","Red","+91 9626719943");
// twilioResp.init();  