// bloodbank.route.js

const express = require('express');
const app = express();
const routes = express.Router();

// Require Contact model in our routes module
let Contact = require('../models/Contact');
let Transaction = require('../models/Transaction');

let TwilioResponse = require('./twilio.route');


var user = "Karuna";
var time = 4;
var bloodBank = "abc";
var toNumber;

// Defined store route
routes.route('/addcontact').post(function (req, res) {
  let contact = new Contact(req.body);

  console.log("contact obj: ", contact);
  console.log("reqbody:", req.body);

  contact.save()
    .then(contact => {
      res.status(200).json({'contact': 'contact in added successfully'});
    })
    .catch(err => {
        res.status(400).send("unable to save to database");
    });
});

routes.route('/raiserequest').post(function (req, res) {
  let transaction = new Transaction(req.body);

  console.log("transaction obj: ", transaction);

  Contact.find(function (err, contacts){
    if(err){
      console.log(err);
    }
    else {
		// success
		console.log("contacts list:", contacts);
      //res.json(businesses);
	  calculateDistance(transaction, contacts, function(res) {
		console.log("res:", res);
		});
    }
  });
  
  transaction.save()
    .then(transaction => {
      res.status(200).json({'transaction': 'transaction placed successfully'});
    })
    .catch(err => {
        res.status(400).send("unable to save to database");
    });
	
	
});

routes.route("/maketwiliocall").get(function(req, res) {
	console.log("routing /maketwiliocall");
	/*var twilioResp = new TwilioResponse("Karuna","4","Red","+91 9626719943");
	twilioResp.init();*/
	
	user = "Vishnu";
	time = 4;
	bloodBank = "abc";
	toNumber = "91 9043487379";
	
	twilioCreateCall();
	
	res.status(200).json({'status': 'success'});
});

routes.route('/statusCallback').post(function (req, res) {
	console.log("Got a StatusCallBack Request");
	console.log(req.body.CallStatus);
	res.set('Content-Type', 'text/xml');
	if(req.body.CallStatus == 'completed'){
		res.send('<Response><Hangup></Hangup></Response>');
	}
	 else{
		res.send('<Response></Response>');
	 } 
});

routes.route('/callback').post(function (req, res) {
	console.log("Speech result ::"+req.body.SpeechResult);
	console.log('Triggering callback');
	res.set('Content-Type', 'text/xml');
	res.send('<Response><Say voice="alice">Thank you. Have a nice day.</Say></Response>');
});

const BASE_URL = "http://3.93.81.11:3000/bloodbank";
const accountSid = 'ACe3771e0463140f0e71b4e17e6ab3e22f';
const authToken = '6650b4a2a7f17cf9804580b3a5314f01';
const client = require('twilio')(accountSid, authToken);

var speechDecision = require('./decisionTree');
const speechDecisionObj = new speechDecision();

routes.route('/partialcallback').post(function (req, res) {
	console.log('Triggering partialcallback');
	let state = req.query.state ;
	console.log("state::"+state);
	if(state == "initial")
		 state="interm";
	else if(state == "interm")
		 state="final";
	else
		state= "initial";

	let se_input = req.body.StableSpeechResult;
	let ue_input = req.body.UnstableSpeechResult;
	console.log("Stable input ::"+se_input);
	console.log("UnStable input::"+ue_input);
	let speechResult = se_input != ""?se_input : ue_input != "" ? ue_input :"";
	console.log("sid"+req.body.CallSid) ;  
	client.calls(req.body.CallSid)
		  .update({method: 'POST', url: BASE_URL+'/makeCall?state='+state+'&speechResp='+speechResult})
		  .then(call => console.log(call.to));
		 
	res.set('Content-Type', 'text/xml');
	res.send('<Response></Response>');
});


var twilioCreateCall = function(){
	client.calls
	.create({
			url: BASE_URL+'/makeCall?state=trigger&speechResp=greet',
			to: toNumber,
			from: '+17013472763',
			statusCallback: BASE_URL+'/statusCallback',
			statusCallbackEvent: ['ringing','initiated','completed'],
			statusCallbackMethod: 'POST',
			method:'POST'
	})
	.then(call => console.log("makeCall::"+call.status));
}

routes.route('/makeCall').post(function (req, res) {
	console.log("details::", user, time, bloodBank);
			console.log('Triggering makeCall');
			console.log(req.query.state);
			let state = req.query.state;
			let speech = req.query.speechResp;
			let say="";
			if(speech){
				let speechArray = speechDecisionObj.getTokensFromSpeech(speech);
				console.log("speechArray"+speechArray.length);
				if(speechArray.length == 1)
					say = getMakeCallResponse(state,speechArray[0].toLowerCase(),user,time,bloodBank);
				else{
				   for(let i=0;i<speechArray.length;i++){
					 if(speechDecisionObj.checkContextAvailable(speechArray[i])){
						say = getMakeCallResponse(state,speechArray[i].toLowerCase(),user,time,bloodBank); 
						break;
					 }
				   }
				}
			}
			else{
				say="Sorry, Could you please come again?";
			}
			let responseText="<Response><Gather input='speech' action='"+ BASE_URL
			+ "/callback' partialResultCallback='" + BASE_URL+ "/partialcallback?state="
			+ state+"'><Say voice='alice'>" + say + "</Say></Gather></Response>";
			console.log(responseText);
			console.log("makeCall Response>>"+responseText);
			res.set('Content-Type', 'text/xml');
			res.send(responseText);
		});

var getMakeCallResponse = function(context,speech,user,time,bloodBank){
	console.log("context and speech>>"+context,speech, user, time, bloodBank);
	let speechOut = speechDecisionObj.predictOutputSpeech(context,speech);
	console.log(speechOut);
	if(speechOut != null && speechOut != ""){
		   speechOut = speechOut.includes("@#user#@") ? speechOut.replace("@#user#@",user):speechOut;
		   speechOut = speechOut.includes("@#time#@") ? speechOut.replace("@#time#@",time):speechOut;
		   speechOut = speechOut.includes("@#bloodbank#@") ? speechOut.replace("@#bloodbank#@",bloodBank):speechOut;
	}
	else {
		speechOut="Sorry, Could you please come again?";
	}
	console.log(speechOut);
	return speechOut;
}

var calculateDistance = function(transaction, contactList) {
	var lat1, lon1, lat2, lon2;
	
	var distancekm = 25;		// 25km
	
	lat1 = transaction.latitude;
	lon1 = transaction.longitude;
	
	var nearByContacts = [];
	for(var i = 0; i < contactList.length; i++) {
		lat2 = contactList[i].latitude;
		lon2 = contactList[i].longitude;
		
		console.log("lat,long::", lat1, lon1, lat2, lon2);
		distance(lat1, lon1, lat2, lon2, function(kilometer) {
			console.log("km:", kilometer);
			if(kilometer <= distancekm) {
				nearByContacts.push(contactList[i]);
			}
		});
	}
	
	console.log("nearByContacts:", nearByContacts);
	for(var i = 0; i < nearByContacts.length; i++) {
		var mobileNo = "+91 " + nearByContacts[i].phone_number;
		var twilioResp = new TwilioResponse(nearByContacts[i].first_name, transaction.within_hour, "Red Group" ,mobileNo);
		twilioResp.init();
	}
};

var distance = function(lat1, lon1, lat2, lon2, callback) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		callback(0);
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		dist = dist * 1.609344
		callback(dist);
	}
};

/*routes.route('/authenticate').post(function (req, res) {

  console.log("email: ", req.body.email);
  console.log("password:", req.body.password);

  Contact.find({"email", req.body.email} )

  contact.save()
    .then(contact => {
      res.status(200).json({'contact': 'contact in added successfully'});
    })
    .catch(err => {
        res.status(400).send("unable to save to database");
    });
});*/


module.exports = routes;